const express = require('express');
const app = express();

app.use(express.static(__dirname + '/assets/'))

app.get('/',function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

app.get('/js', function (req,res) {
    res.send('Node.js');
});

app.listen(8080, function() {
    console.log("listening on port 8080");
});

